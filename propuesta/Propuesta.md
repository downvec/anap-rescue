% Propuesta de proyecto
% Equipo JJAR (Julieta, José, Alonso, Rebe)

# Optimizando calificaciones para maximizar la nota final
## O: ¿Por fin voy a pasar eco 2?

# Planteamiento

## Función de calificación

Queremos encontrar la mayor calificación final posible que podríamos obtener
dado que ya conocemos la calificación del 'primer parcial'.

Para no caer en un problema lineal consideramos funciones de
calificación no-lineales.

. . .

Por ejemplo: Peso de parciales depende de si es el más alto o bajo

$$
\frac{1}{5} p_{\text{lo}} + \frac{2}{5} \left( \sum_{i} p_{i} \right) + \frac{2}{5} y
$$

donde $y$ representa el examen final, $p_{\text{lo}}$ el parcial más
bajo y $\sum_{i} p_{i}$ es la suma de los demás parciales.

----

![Calificación final en función de parciales y final](figs/cs_simple_xkcd.png){.stretch}

----

# ¿Qué tan probable es tener ___ de calificación Final?

Asumiendo que las calificaciones se distribuyen gamma (explicar por qué gamma)

![Distribución gamma con curvas de nivel en el piso](figs/normal_3d.png){.stretch}

## Juntando ambas ideas

![](figs/cs_compuesto_xkcd.png){.stretch}

----

![](figs/probab_xkcd.png){.stretch}

----

# ¿Cuál es nuestra función a optimizar?

Función score: calcula las posibles calificaciones finales dados los datos,
criterios de calificación y calificaciones dadas, ponderando por la probabilidad
de obtener dicha calificación.

Vectores a optimizar: x, y

x: evaluaciones restantes

y: calificación del final

----

# ¿Qué nos regresa la optimización?

Obtenemos la máxima posible calificación * probabilidad de sacarla.

----

# ¿Cómo optimizamos?

Algoritmos de Búsqueda Lineal vs. Región de Confianza ¿?

----

# Resultados
