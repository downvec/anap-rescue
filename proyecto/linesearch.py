import numpy as np
from derivadas import Grad, Hess
from condiciones import s_o_c, f_o_c
from wolfe import genera_alpha

import numdifftools as nd

from icecream import ic


class OptimizeResult:
    """ Representa el resultado del proceso de optimización

    Attributes:
        x_star: array_like
            Punto resultante de la optimización
        success: bool
            Si se optimizó bien
        nit: int
            Número de iteraciones del algoritmo
    """

    def __init__(self, x_star, success, nit) -> None:
        self.x_star = x_star
        self.success = success
        self.nit = nit

    def __repr__(self) -> str:
        if self.success:
            msg = f"Se optimizó exitosamente la función en {self.nit} iteraciones con resultado en {self.x_star}"
        else:
            msg = f"No se logró optimizar la función en {self.nit} iteraciones"

        return msg


def BL(f, x0, method="Busqueda Lineal"):
    """
    Algoritmo de Newton (sin recorte de alpha)
    """
    # Inicializando el punto
    xk = x0
    # Cuento iteraciones porque el chiste es compararlos
    niters = 0
    # Tambien pongo iters maximos para que se vea que Newton falla
    max_iters = 200

    # Uso la misma función porque casi todo el código es igual salvo generación de alpha
    if method == "max-desc":
        max_iters = 9000
        alpha_gen = lambda x, pk_N: genera_alpha(f, x0, pk_N)
        get_desc_dir = lambda grad, hess: -grad
    elif method == "newton":
        alpha_gen = lambda x, pk_N: genera_alpha(f, x, pk_N)
        get_desc_dir = lambda grad, hess: np.linalg.solve(hess, -grad)
    else:  # Asumo que el default es busqueda lineal
        alpha_gen = lambda x, pk_N: genera_alpha(f, x, pk_N)
        get_desc_dir = lambda grad, hess: np.linalg.solve(hess, -grad)

    while not (f_o_c(f, xk) and s_o_c(f, xk) and niters <= max_iters):
        grad = nd.Gradient(f)(xk)
        # grad = np.gradient(f(xk))
        hess = nd.Hessian(f)(xk)

        ic(grad)

        # En ambos casos usamos p_k^N como dirección de descenso
        pk_N = get_desc_dir(grad, hess)
        alpha = alpha_gen(xk, pk_N)
        xk = xk + alpha * pk_N

        # ic(f"xk: {xk} k: {niters} pk: {pk_N} a:{alpha}")
        ic(xk, niters, pk_N, alpha)

        niters += 1

    return OptimizeResult(xk, niters <= max_iters, niters)


# def BL(f, x0, method="Busqueda Lineal"):
#     """
#     Algoritmo de Newton (sin recorte de alpha)
#     """
#     # Inicializando el punto
#     xk = x0
#     # Cuento iteraciones porque el chiste es compararlos
#     niters = 0
#     # Tambien pongo iters maximos para que se vea que Newton falla
#     max_iters = 10000

#     while not (f_o_c(f, xk) and s_o_c(f, xk) and niters <= max_iters):

#         # Uso la misma función porque casi todo el código es igual salvo generación de alpha
#         if method == "Newton":
#             # "centro" generación de alpha en algún punto constante para que no cambie.
#             # No importa si no es un valor bonito, solo me importa que no cambie
#             alpha_centro = x0
#             max_iters = 9000
#         elif method == "Busqueda Lineal":
#             alpha_centro = xk
#         else:  # Asumo que el default es busqueda lineal
#             alpha_centro = xk

#         grad = Grad(f, xk)
#         hess = Hess(f, xk)
#         # En ambos casos usamos p_k^N como dirección de descenso
#         pk_N = np.linalg.solve(hess, -grad)
#         alpha = genera_alpha(f, alpha_centro, pk_N)
#         xk = xk + alpha * pk_N

#         print(f"xk: {xk} k: {niters} pk: {pk_N} a:{alpha}")

#         niters += 1

#     print(f"{niters} iteraciones con metodo {method}")
#     return xk
