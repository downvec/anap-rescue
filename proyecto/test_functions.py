import numpy as np
from derivadas import Grad, funcion_costos, Hess
from scipy.optimize import rosen_hess

res = np.array([0, 0])
print(rosen_hess([1, 1]))
print("----")
print(Grad(funcion_costos, [1, 1]))
b = Grad(funcion_costos, [1, 1])
print(np.isclose([0, 0], b, atol=1e-6))
print(all(np.isclose([0, 0], b, atol=1e-6)))

H = Hess(funcion_costos, [1, 1])

print(type(H), H)
b = np.matrix([[802, -400], [-400, 200]])
print(np.isclose(H, b, atol=1e-4).all())
# A1 = np.squeeze(np.asarray(H))
# A2 = np.squeeze(np.asarray(b))


def test_Grad():
    b = Grad(funcion_costos, [1, 1])
    assert all(np.isclose([0, 0], b, atol=1e-6))


def test_Hessian():
    H = Hess(funcion_costos, [1, 1])
    b = np.matrix([[802, -400], [-400, 200]])
    assert np.isclose(H, b, atol=1e-4).all()
