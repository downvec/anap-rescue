from math import sqrt

import numpy as np
from numpy import linalg as ln

import matplotlib.pyplot as plt
import seaborn as sns
from condiciones import is_min
from scipy.optimize import rosen

from matplotlib import ticker

from icecream import ic

sns.set()


# Jacobiano analítico sacado a mano
def jaco(x):
    return np.array(
        [-400 * (x[1] - x[0] ** 2) * x[0] - 2 + 2 * x[0], 200 * x[1] - 200 * x[0] ** 2]
    )


# Hessiana analítica sacada a mano
def hesso(x):
    return np.array(
        [[1200 * x[0] ** 2 - 400 * x[1] + 2, -400 * x[0]], [-400 * x[0], 200]]
    )


def step_cauchy(gk, Bk, delta):
    tau_k = 1

    # multi_dot conjuga como queremos y además multiplica en orden óptimo
    cuadratica = ln.multi_dot([gk, Bk, gk])
    g_norm = ln.norm(gk)

    if cuadratica <= 0:
        tau_k = 1
    else:
        tau_k = min(g_norm ** 3 / delta * cuadratica, 1)

    # Calculando punto de Cauchy
    factor = (tau_k * delta) / g_norm
    p_C = -1 * factor * gk

    return p_C


def step_dogleg(Hk, gk, Bk, delta):
    """
    Encuentra paso del método dogleg de acuerdo a Nocedal

    Args:
        Hk: Inversa de la matriz Bk
        gk: Gradiente de f en xk
        Bk: Hessiana en xk o aproximación simétrica
        delta: Radio de la región de confianza

    Returns: np.array con el paso óptimo
    """

    # Calculamos el full step
    pB = -np.dot(Hk, gk)
    norm_pB = ln.norm(pB)

    # Si full step está en la región, lo regresamos
    if norm_pB <= delta:
        return pB

    # Calculamos pU
    pU = -(np.dot(gk, gk) / ln.multi_dot([gk, Bk, gk])) * gk
    dot_pU = np.dot(pU, pU)
    norm_pU = ln.norm(pU)

    # Si pU se sale de la región, usamos el punto de intersección con la frontera
    if norm_pU >= delta:
        return delta * pU / norm_pU

    # De otra forma resolvemos el camino óptimo resolviendo la ecuación cuadrática como en Nocedal.
    # Es decir: ||pU + (tau - 1)(pB - pU)||^2 = delta^2
    # Usando la fórmula cuadrática
    pB_pU = pB - pU

    dot_pB_pU = np.dot(pB_pU, pB_pU)
    dot_pU_pB_pU = np.dot(pU, pB_pU)

    fact = dot_pU_pB_pU ** 2 - dot_pB_pU * (dot_pU - delta ** 2)
    tau = (-dot_pU_pB_pU + sqrt(fact)) / dot_pB_pU

    # Eligiendo de acuerdo a Nocedal pg.74 problema 4.16
    return pU + tau * pB_pU


def region_confianza(
    func,
    jac,
    hess,
    x0,
    metodo="Dogleg",
    delta_0=1.0,
    delta_max=100.0,
    eta=0.15,
    maxiter=50,
):
    pts = np.zeros((2000, len(x0) + 2))
    xk = x0
    delta_k = delta_0
    k = 0
    while not is_min(jac, hess, xk) and k <= maxiter:
        gk = jac(xk)
        Bk = hess(xk)
        Hk = np.linalg.inv(Bk)

        # Book-keeping para graficar
        pts[k] = np.append(xk, [k, np.exp(delta_k)])

        # Seleccionamos pk con el Punto de Cauchy
        if metodo == "Cauchy":
            pk = step_cauchy(gk, Bk, delta_k)
        elif metodo == "Dogleg":
            pk = step_dogleg(Hk, gk, Bk, delta_k)

        # Reducción real
        red_re = func(xk) - func(xk + pk)
        # reducción esperada
        red_esp = -(np.dot(gk, pk) + 0.5 * ln.multi_dot([pk, Bk, pk]))

        # Rho.
        rho_k = red_re / red_esp

        # Evitando caso esquina
        if red_esp == 0.0:
            rho_k = 1e99
        else:
            rho_k = red_re / red_esp

        norm_pk = ln.norm(pk)

        # Rho es muy pequeño, reducimos el radio de la región de confianza
        if rho_k < 0.25:
            delta_k = 0.25 * norm_pk
        else:
            # Rho ~= 1 y pk está en la frontera de la región. Expandimos el radio
            if rho_k > 0.75 and norm_pk == delta_k:
                delta_k = min(2.0 * delta_k, delta_max)

        # Actualizando el paso
        if rho_k > eta:
            xk = xk + pk
        else:
            xk = xk

        ic(k, xk, pk, gk)

        k += 1

    return xk, pts


if __name__ == "__main__":
    x0 = np.array([5, 5])

    xk, pts = region_confianza(rosen, jaco, hesso, x0)

    X, Y = np.mgrid[-3:6:0.1, -5:8:0.1]

    plt.contourf(X, Y, rosen([X, Y]), locator=ticker.LogLocator(), cmap="Blues")

    sns.scatterplot(x=pts[:, 0], y=pts[:, 1])

    plt.show()
